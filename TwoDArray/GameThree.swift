//
//  TwoDArray
//
//  Created by Beuniq on 17/07/2017.
//  Copyright © 2017 Beuniq. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

enum gameState
{
    case inGame
    case pause
    case gameOver
}

extension SKLabelNode {
    func multilined(lineSpacing : CGFloat) -> SKLabelNode {
        let substrings: [String] = self.text!.components(separatedBy: "\n")
        //        print("substrings : \(substrings)")
        var y : CGFloat = 0
        return substrings.enumerated().reduce(SKLabelNode()) {
            let label = SKLabelNode(fontNamed: self.fontName)
            label.text = $1.element
            label.fontColor = self.fontColor
            label.fontSize = self.fontSize
            label.position = self.position
            label.horizontalAlignmentMode = self.horizontalAlignmentMode
            label.verticalAlignmentMode = self.verticalAlignmentMode
            //if not the first then move the other text below the origin one
            if($1.offset != 0)
            {
                y += (CGFloat(lineSpacing) + CGFloat(self.fontSize))
            }
            label.position = CGPoint(x: 0, y: -y)
            $0.addChild(label)
            return $0
        }
    }
    
    convenience init?(fontNamed font: String, andText text: String, andSize size: CGFloat, withShadow shadow: UIColor) {
        self.init(fontNamed: font)
        self.text = text
        self.fontSize = size
        
        let shadowNode = SKLabelNode(fontNamed: font)
        shadowNode.text = self.text
        shadowNode.zPosition = self.zPosition - 1
        shadowNode.fontColor = shadow
        // Just create a little offset from the main text label
        shadowNode.position = CGPoint(x: 4, y: -4)
        shadowNode.fontSize = self.fontSize
        shadowNode.alpha = 0.5
        
        self.addChild(shadowNode)
    }
}

class GameThree: SKScene {
    
    let targetRow = 12
    let targetCol = 15
    var currentGameState = gameState.inGame
    var currentGameLvl = 0
    
    var mapTemp = [Int]()
    var mapNumber = [[Int]]()
    var boolMapOrigin = [[Bool]]()
    var boolMapTemp = [[Bool]]()
    
    var playAbleWidth : CGFloat = 0
    var startingNum : Int = 0
    var currentColorNumber : Int = 0
    
    //some default stuff
    var timeLabel = SKLabelNode()
    var secTime : Int = 0
    var gameTimer = Timer()
    var accBonusScore : Int = 0
    var highscore : Int = 0
    var bgIntro = SKSpriteNode()
    
    var introTimeLabel = SKLabelNode()
    var introTimeCount = 3
    var introTimer = Timer()
    
    var moveLabel = SKLabelNode()
    var moveCount : Int = 0
    var maxMoveCount : Int = 0
    
    var progressLabel = SKLabelNode()
    var progressCount : Int = 0
    
    var defDurationPressBar : CGFloat = 5.0
    var durationToPressBtn : CGFloat = 0.0
    var progressPressBar = SKSpriteNode()
    var pressTimer = Timer()
    
    var gameEnd = false
    var winGame = false
    var XpressBtnEnd = false
    var endGameButton = SKSpriteNode()
    var rect1_top = SKShapeNode()
    var rect2_top = SKShapeNode()
    var countTutorial = 0
    
    var msgContinue = ["Informed Decision Making \nhelps you make better choices \nand be a better leader.",
                       "Experimenting keeps us open to\nbetter ways of doing things and \nembracing creative approaches.",
                       "Simplicity allows for easier \nunderstanding,execution and\nefficiency in our daily lives.",
                       "An Agile Mindset would allow us \nto embrace change easier in\norder to excel in trying times.",
                       "Fast Execution lets us catch \nopportunities as they come and \nimproves upon our work efficiency." ,
                       "Being Hyper Aware helps us \nto adapt and act according to new \ncircumstances and information.",
                       "Teamwork gives us the ability to \nleverage on our shared strengths and \ncounter our personal shortcomings.",
                       "A Data Driven individual will find it \neasier to connect the dots in trends, \nactions and results to improve \nthemselves or the organisation." ]
    
    public static var iconSpriteName = [String]()
    public static var iconName = [String]()
    
    public var textureIconName = ["icon_agile mindset","icon_collaborative","icon_customer obsessed","icon_data driven","icon_experimenting","icon_fast execution","icon_hyper aware", "icon_informed decision making","icon_innovative","icon_keep up", "icon_persistent","icon_simplicity","icon_social media savvy","icon_team work"]
    public var textForBtn = ["Agile Mindset", "Collaborative","Customer Obsessed","Data Driven","Experimenting","Fast Execution","Hyper Aware","Informed decision \nMaking","Innovative","Keep Up","Persistent","Simplicity","Social Media\nSavvy","Team Work"]
    
    var bgSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "POL-jungle-hideout-short", ofType: "wav")!)
    var audioPlayer = AVAudioPlayer()
    
    //node
    var btnNode = SKNode()
    var btnNode2 = SKNode()
    var mapNode = SKNode()
    var introNode = SKNode()
    var UIGameOverNode = SKNode()
    let userDefults = UserDefaults.standard //returns shared defaults object.
    
    override func didMove(to view: SKView) {
        
        //setting up the bg
        backgroundColor = SKColor.blue
        let bg = SKSpriteNode(imageNamed: "bg")
        bg.size = self.size
        bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bg.zPosition = -1
        addChild(bg)
        
        //getting the aspect ratio of the ui of the apps using
        let screenSize2 = UIScreen.main.bounds
        let maxAspectRatio: CGFloat = screenSize2.height / screenSize2.width
        playAbleWidth = size.height / maxAspectRatio
        
        currentGameState = gameState.inGame
        
        getHighscore()
        
        do {
            
            audioPlayer = try AVAudioPlayer(contentsOf: bgSound as URL)
            audioPlayer.play()
            audioPlayer.numberOfLoops = -1
            audioPlayer.volume = 0.2
            
        } catch {
            
            print("Problem in getting File")
        }
        
        GameThree.iconName.removeAll()
        GameThree.iconSpriteName.removeAll()
        
        GameThree.iconName = textForBtn
        GameThree.iconSpriteName = textureIconName
        
        //get the game level
        //if the player lose before, the game level will be reset to 0
        if( userDefults.object(forKey: "gameLvl") as? Int != nil)
        {
            currentGameLvl = userDefults.object(forKey: "gameLvl") as! Int
        }
        
        if(userDefults.object(forKey: "accumulatedBonus") as? Int != nil)
        {
            accBonusScore = userDefults.object(forKey: "accumulatedBonus") as! Int
        }
        
        if(userDefults.object(forKey: "elapseTime") as? Int != nil)
        {
            secTime = userDefults.object(forKey: "elapseTime") as! Int
        }
        
        getHighscore()
        
        //the press duration max
        if((Int(defDurationPressBar) - currentGameLvl) < 3)
        {
            defDurationPressBar = 2
        }
        else
        {
            defDurationPressBar -= CGFloat(currentGameLvl)
        }
        
        //the movecount for current level
        if((40 - (currentGameLvl*5)) < 25)
        {
            maxMoveCount = 25
        }
        else
        {
            maxMoveCount = 40 - (currentGameLvl * 5)
        }
        
        //        print(defDurationPressBar)
        
        //        progressPressBar = SKSpriteNode(color: UIColor(red: 101/255.0, green: 224/255.0, blue: 196/255.0, alpha: 1.0), size: CGSize(width: 975, height: 50))
        progressPressBar = SKSpriteNode(imageNamed: "bar_timer")
        progressPressBar.color = UIColor(red: 78/255.0, green: 255/255.0, blue: 160/255.0, alpha: 1.0)
        progressPressBar.colorBlendFactor = 1
        progressPressBar.position = CGPoint(x: self.size.width/2 - CGFloat(CGFloat(targetCol/2) * CGFloat(65 + 5)), y: self.frame.size.height/2 - 290)
        progressPressBar.anchorPoint = CGPoint(x:0.0, y:0.5)
        progressPressBar.xScale = 1
        progressPressBar.zPosition = 4
        addChild(progressPressBar)
        
        //        let bgProgressPressBar = SKSpriteNode(color:SKColor.white, size: CGSize(width: 975, height: 50))
        let bgProgressPressBar = SKSpriteNode(imageNamed: "bar_timer")
        bgProgressPressBar.position = CGPoint(x: self.size.width/2 - CGFloat(CGFloat(targetCol/2) * CGFloat(65 + 5)), y: self.frame.size.height/2 - 290)
        bgProgressPressBar.anchorPoint = CGPoint(x:0.0, y:0.5)
        bgProgressPressBar.zPosition = 3
        addChild(bgProgressPressBar)
        
        let clockTimer = SKSpriteNode(imageNamed: "clock")
        
        timeLabel = SKLabelNode(fontNamed: "AllerDisplay")
        timeLabel.fontSize = 80
        timeLabel.zPosition = 13
        timeLabel.fontColor = UIColor(red: 88/255.0, green: 136/255.0, blue: 184/255.0, alpha: 1.0)
        timeLabel.position = CGPoint(x: size.width/2 + (clockTimer.size.width/2), y: size.height - 95 )
        //        timeLabel.text = "00:00"
        timeLabel.text = timeString(time: TimeInterval(secTime))
        addChild(timeLabel)
        
        clockTimer.zPosition = 13
        clockTimer.position = CGPoint(x: timeLabel.position.x - 170 , y: size.height - 70)
        addChild(clockTimer)
        
        moveLabel = SKLabelNode(fontNamed: "H.H. Samuel")
        moveLabel.fontSize = 55
        moveLabel.zPosition = 13
        moveLabel.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        moveLabel.position = CGPoint(x: self.size.width/2 - 150 , y: self.size.height - 260)
        moveLabel.text = "Move : \(moveCount) / \(maxMoveCount)"
        addChild(moveLabel)
        
        progressLabel = SKLabelNode(fontNamed: "H.H. Samuel")
        progressLabel.fontSize = 55
        progressLabel.zPosition = 13
        progressLabel.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        progressLabel.position = CGPoint(x: self.size.width/2 + 200 , y: self.size.height - 260)
        addChild(progressLabel)
        
        rect1_top = SKShapeNode(rect: CGRect(x:self.size.width/2 - (720/2) + 50  , y: self.size.height - 330 , width: 720, height: 176),cornerRadius: 20)
        rect1_top.fillColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1_top.strokeColor = UIColor(red: 211/255.0, green: 229/255.0, blue: 245/255.0, alpha: 1.0)
        rect1_top.zPosition = 10
        addChild(rect1_top)
        
        rect2_top = SKShapeNode(rect: CGRect(x:self.size.width/2 - (668/2) + 50  , y: self.size.height - 300, width: 668, height: 128),cornerRadius: 20)
        rect2_top.fillColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2_top.strokeColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        rect2_top.zPosition = 11
        addChild(rect2_top)
        
        let windowRight = SKSpriteNode(imageNamed: "window")
        windowRight.zPosition = 1
        //        (frame.midX + (playAbleWidth/2) - 40
        windowRight.position = CGPoint(x:(frame.midX + (576.901) - 40) , y: self.size.height - 100 - windowRight.size.height/2) //self.size.width + 10
        addChild(windowRight)
        
        let worker_sit = SKSpriteNode(imageNamed: "game03_worker")
        worker_sit.position = CGPoint(x: frame.midX - (576.901) + 130, y: self.size.height - 280)
        worker_sit.zPosition = 13
        addChild(worker_sit)
        
        addChild(introNode)
        addChild(btnNode)
        addChild(btnNode2)
        addChild(mapNode)
        addChild(UIGameOverNode)
        
        introTimeLabel = SKLabelNode(fontNamed: "AllerDisplay")
        introTimeLabel.fontSize = 200
        introTimeLabel.zPosition = 130
        introTimeLabel.fontColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        introTimeLabel.position = CGPoint(x: frame.midX , y: frame.midY  )
        introTimeLabel.text = "\(introTimeCount)"
        introNode.addChild(introTimeLabel)
        
        bgIntro = SKSpriteNode(color: UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50), size: CGSize(width: playAbleWidth, height: self.size.height))
        bgIntro.position = CGPoint(x: frame.midX , y: frame.midY)
        bgIntro.zPosition = 97
        introNode.addChild(bgIntro)
        
        setup2DArray()
        setUpBtn()
        
        introTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimerIntro), userInfo: nil, repeats: true)
    }
    
    func updateTimerIntro() //timer ticking for the intro
    {
        introTimeCount -= 1
        introTimeLabel.text = "\(introTimeCount)"
        if(introTimeCount == 0)
        {
            introTimer.invalidate()
            //if reach 0 stop the time n straight start the game
            if(highscore == 0 && currentGameLvl == 0)
            {
                countTutorial = 0
                currentGameState = gameState.pause
                tutorialChange()
            }
            else
            {
                introNode.removeAllChildren()
                introNode.removeFromParent()
                startGameTimer()
                resetPressTimerFunc(starting: true)
                
            }
        }
    }
    
    var arrowPoint = SKSpriteNode(imageNamed: "arrow.png")
    var extra_text_tutorial = SKLabelNode(fontNamed: "H.H. Samuel")
    func tutorialChange()
    {
        countTutorial += 1
        
        if(countTutorial == 1)
        {
            for child in btnNode2.children
            {
                if let spriteNode = child as? GameSprite
                {
                    spriteNode.zPosition = 97
                }
            }
            for child in btnNode.children
            {
                if let spriteNode = child as? GameSprite
                {
                    spriteNode.zPosition = 99
                }
            }

            
            var tempPosition = CGPoint(x: 0, y: 0)
            
            for x in 0..<mapNode.children.count
            {
                if let grid = mapNode.children[x] as? GameSprite
                {
                    if(x == 0)
                    {
                        tempPosition = grid.position
                        break;
                    }
                }
            }
           
            bgIntro.zPosition = 90
            
            arrowPoint.position = CGPoint(x: tempPosition.x + 44, y: tempPosition.y + CGFloat(64/2) + (arrowPoint.size.width) + 10)
            arrowPoint.zPosition = 100
            introNode.addChild(arrowPoint)
            
            extra_text_tutorial.text = "You start from here"
            extra_text_tutorial.position = CGPoint(x: tempPosition.x + (extra_text_tutorial.frame.width), y: arrowPoint.position.y + (arrowPoint.size.width/2) + (extra_text_tutorial.frame.height) + 40)
            extra_text_tutorial.fontSize = 80
            extra_text_tutorial.zPosition = 100
            extra_text_tutorial.fontColor = UIColor.white
            introNode.addChild(extra_text_tutorial)
            
            let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
            temp_msg.text = "Your mission is to change your color\nto match with adjancent color\nby clicking the button below"
            temp_msg.fontSize = 75
            temp_msg.horizontalAlignmentMode = .center
            temp_msg.fontColor = UIColor.white
            
            introTimeLabel.removeFromParent()
            introTimeLabel = temp_msg.multilined(lineSpacing: 10)
            introTimeLabel.position = CGPoint(x: frame.midX, y: frame.midY - 120)
            introTimeLabel.zPosition = 100
            introNode.addChild(introTimeLabel)
        }
        else if(countTutorial == 2)
        {
            for child in btnNode2.children
            {
                if let spriteNode = child as? GameSprite
                {
                    spriteNode.zPosition = 18
                }
            }
            for child in btnNode.children
            {
                if let spriteNode = child as? GameSprite
                {
                    spriteNode.zPosition = 20
                }
            }
            
            extra_text_tutorial.removeFromParent()
            rect1_top.zPosition = 91
            rect2_top.zPosition = 92
            moveLabel.zPosition = 93
            progressLabel.zPosition = 93
            
            arrowPoint.position = CGPoint(x: moveLabel.position.x - 220 , y: moveLabel.position.y - 100)
            arrowPoint.zRotation = CGFloat(180) * CGFloat(M_PI) / 180.0
            
            let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
            temp_msg.text = "This is your step used and progressed achived\nOnce you run out of step, the game end"
            temp_msg.fontSize = 75
            temp_msg.horizontalAlignmentMode = .center
            temp_msg.fontColor = UIColor.white
            
            introTimeLabel.removeFromParent()
            introTimeLabel = temp_msg.multilined(lineSpacing: 10)
            introTimeLabel.position = CGPoint(x: frame.midX, y: frame.midY + 500)
            introTimeLabel.zPosition = 100
            introNode.addChild(introTimeLabel)
        }
        
        else if(countTutorial == 3)
        {
            rect1_top.zPosition = 10
            rect2_top.zPosition = 11
            moveLabel.zPosition = 13
            progressLabel.zPosition = 13
            
            progressPressBar.zPosition = 91
            
            arrowPoint.position = CGPoint(x: progressPressBar.position.x + progressPressBar.size.width + 40 , y: progressPressBar.position.y + 90)
            arrowPoint.zRotation = 0
            
            let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
            temp_msg.text = "This is the timer for each turn\nWhen time runs out, the game end"
            temp_msg.fontSize = 75
            temp_msg.horizontalAlignmentMode = .center
            temp_msg.fontColor = UIColor.white
            
            introTimeLabel.removeFromParent()
            introTimeLabel = temp_msg.multilined(lineSpacing: 10)
            introTimeLabel.position = CGPoint(x: frame.midX, y: frame.midY - 100)
            introTimeLabel.zPosition = 100
            introNode.addChild(introTimeLabel)
        }
        else if(countTutorial == 4)
        {
            progressPressBar.zPosition = 4
            introNode.removeAllChildren()
            introNode.removeFromParent()
            startGameTimer()
            resetPressTimerFunc(starting: true)
            currentGameState = gameState.inGame
        }
    }
    
    func startGameTimer()
    {
        gameTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateGameTimer), userInfo: nil, repeats: true)
    }
    
    func updateGameTimer()
    {
        secTime += 1
        timeLabel.text = timeString(time: TimeInterval(secTime))
    }
    
    func timeString(time:TimeInterval) -> String {
        //        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func resetPressTimerFunc(starting : Bool)
    {
        progressPressBar.removeAllActions()
        progressPressBar.xScale = 1
        pressTimer.invalidate()
        
        if(starting == false) //when game started, no need to calculate the mark
        {
            checkMarkTimePress()
        }
        
        durationToPressBtn = 0
        let action_1 = SKAction.scaleX(to: 0, duration: TimeInterval(self.defDurationPressBar))
        let action_2 = SKAction.run {
            print("dead")
            self.XpressBtnEnd = true
            self.endGame(gameWin: false)
        }
        pressBtn()
        progressPressBar.run(SKAction.sequence([action_1,action_2]))
    }
    
    func pressBtn()
    {
        durationToPressBtn = 0
        pressTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.minusCheckPress), userInfo: nil, repeats: true)
    }
    
    func minusCheckPress()
    {
        durationToPressBtn += 0.1
        //        print(durationToPressBtn)
    }
    
    func checkMarkTimePress()
    {
        let temp = defDurationPressBar/CGFloat(2)
        var markToAdd = 0
        let checkMark = defDurationPressBar - durationToPressBtn
        
        //        print("temp : \(temp)")
        for i in 1...2
        {
            if(checkMark <= CGFloat(temp * CGFloat(i)))
            {
                markToAdd = i-1
                break;
            }
        }
        //        print("press at duration : \(checkMark) and got mark : \(markToAdd)")
        
        run(SKAction.playSoundFileNamed("switch21.wav", waitForCompletion: false))
        
        let timePlus = SKLabelNode(fontNamed: "AllerDisplay", andText: "+\(markToAdd)s", andSize: 120, withShadow: UIColor(red: 33/255.0, green: 44/255.0, blue: 91/255.0, alpha: 1.0))
        timePlus?.zPosition = 1000
        timePlus?.position = CGPoint(x: (progressPressBar.position.x + (progressPressBar.size.width/2)), y: (progressPressBar.position.y) + 30)
        addChild(timePlus!)
        
        let moveUp = SKAction.moveBy(x: 0, y: +50, duration: 0.5)
        let removeActionText = SKAction.removeFromParent()
        timePlus?.run(SKAction.sequence([moveUp,removeActionText]))
        
        secTime += markToAdd
        timeLabel.text = timeString(time: TimeInterval(secTime))
    }
    
    func setup2DArray()
    {
        var tempFirstNum = 0
        for i in 0..<targetRow {
            var subArray = [Int]()
            var subArray_2 = [Bool]()
            var subArray_3 = [Bool]()
            for j in 0..<targetCol {
                
                var a = Int(arc4random_uniform(UInt32(6)) + 1) //inside Uint32 is range, and +
                if(i == 0 && j == 0)
                {
                    tempFirstNum = a
                    subArray_2.append(true)
                }
                else
                {
                    subArray_2.append(false)
                }
                
                if(((i == 0 && j == 1) && a == tempFirstNum) || ((i == 1 && j == 0) && a == tempFirstNum))
                {
                    while(a == tempFirstNum)
                    {
                        a = Int(arc4random_uniform(UInt32(6)) + 1)
                    }
                }
                
                subArray_3.append(false)
                subArray.append(a)
                mapTemp.append(a)
            }
            mapNumber.append(subArray)
            boolMapOrigin.append(subArray_2)
            boolMapTemp.append(subArray_3)
        }
        
        currentColorNumber = tempFirstNum
        
        let tempEgMapGridSmall : GameSprite = GameSprite(spriteRow: -1, spriteColumn: -1, spriteNum: 1, currentSpriteType: GameSpriteType.grid)
        let positionXStart : CGFloat = self.size.width/2 - CGFloat(CGFloat(targetCol/2) * CGFloat(tempEgMapGridSmall.size.width)) - 1.0
        let positionYStart : CGFloat = self.size.height - tempEgMapGridSmall.size.height/2 - 460
        
        for i in 0..<mapNumber.count // y
        {
            for j in 0..<mapNumber[i].count // x
            {
                let tempSmallGridMap : GameSprite = GameSprite(spriteRow: i, spriteColumn: j, spriteNum: mapNumber[i][j], currentSpriteType: GameSpriteType.grid)
                tempSmallGridMap.position = CGPoint(x: positionXStart + CGFloat((tempEgMapGridSmall.size.width ) * CGFloat(j)) , y: positionYStart - CGFloat((tempEgMapGridSmall.size.height) * CGFloat(i)) )
                tempSmallGridMap.name = "X:\(i) , Y: \(j)"
                //                tempEgMapGridSmall.changeGameGridNum(number : mapNumber[i][j])
                mapNode.addChild(tempSmallGridMap)
            }
        }
        
        for x in 0..<mapNumber.count{
            var line = ""
            for y in 0..<mapNumber[x].count
            {
                line += String(mapNumber[x][y])
                line += " "
            }
            print(line)
        }
        
        let percentage = Int((CGFloat(progressCount) / CGFloat(mapTemp.count)) * 100)
        progressLabel.text = "Progress : \(percentage) / \(100)"
    }
    
    func setUpBtn()
    {
        let positionXStart : CGFloat = self.size.width/2 - 30
        let positionYStart : CGFloat = self.size.height/2 - 350
        let btnXSpacing : CGFloat = 32.0
        let btnYSpacing : CGFloat = 30.0
        var tempCount = 0
        
        for y in 0..<2
        {
            for x in 0..<3
            {
                tempCount += 1
                
                let btnPressed : GameSprite = GameSprite(spriteRow: y, spriteColumn: x, spriteNum: tempCount, currentSpriteType: GameSpriteType.gameBtn)
                btnPressed.name = "btn_can_be_pressed"
                btnPressed.position = CGPoint(x: (positionXStart - ((btnPressed.size.width + 10))) + CGFloat((btnPressed.size.width + btnXSpacing) * CGFloat(x)), y: (positionYStart - btnPressed.size.height/2) - CGFloat((btnPressed.size.height + btnYSpacing) * CGFloat(y)))
                btnPressed.zPosition = 18
                
                let imgBtnSprite : GameSprite = GameSprite(spriteRow: y, spriteColumn: x, spriteNum: tempCount, currentSpriteType: GameSpriteType.gameBtnOverlay)
                imgBtnSprite.position = btnPressed.position
                imgBtnSprite.zPosition = 20
                
                //                print("size : \(GameScene.iconName.count)")
                
                btnNode.addChild(imgBtnSprite)
                btnNode2.addChild(btnPressed)
            }
        }
        
        for child in btnNode2.children
        {
            if let spriteNode = child as? GameSprite
            {
                spriteNode.changeDefBtn()
            }
        }
        
        updateBtnSprite()
    }
    
    var btnPressed = false
    var btnGameOverTouched = false
    var prevNum = 0
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first // get the first touch
        let location = touch?.location(in: self)
        self.view?.isMultipleTouchEnabled = false //disable multiple touch
        
        if(currentGameState == gameState.inGame)
        {
            //if the current touch at the location is a gameSprite
            if let btnSprite = atPoint(location!) as? GameSprite
            {
                //check if we press the gamesprite is a button, then do action
                if(btnSprite.checkBtnGameSprite() == true && btnSprite.getGameSpriteNum() != currentColorNumber && moveCount < maxMoveCount)
                {
                    //do smth
                    btnPressed = true
                    prevNum = btnSprite.getGameSpriteNum()
                }
            }
        }
            
        else if(currentGameState == gameState.gameOver)
        {
            let touchedNode = self.atPoint(location!)
            if let name = touchedNode.name //if the touchnode got name
            {
                if name == endGameButton.name //n its the endgameButton name
                {
                    //change the button sprite and make press button true
                    endGameButton.texture = SKTexture(imageNamed: "btn_continue_02")
                    btnGameOverTouched = true
                }
            }
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first
        let location = touch?.location(in: self)
        if(currentGameState == gameState.inGame)
        {
            if(btnPressed == true)
            {
                let checkBtnSprite = self.atPoint(location!) as? GameSprite
                //if the finger still move inside the same game btn, so btn pressed is counted as true
                if(checkBtnSprite?.checkBtnGameSprite() == true && checkBtnSprite?.getGameSpriteNum() == prevNum)
                {
                    btnPressed = true
                }
                else
                {
                    btnPressed = false
                }
                
            }
        }
            
        else if(currentGameState == gameState.gameOver)
        {
            if(btnGameOverTouched == true)
            {
                //recheck again, if the finger moved out from the game over button, then change sprite and the game cant be continue
                let touchedNode = self.atPoint(location!)
                if let name = touchedNode.name
                {
                    if name != endGameButton.name
                    {
                        btnGameOverTouched = false
                        endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                    }
                }
                    
                else
                {
                    btnGameOverTouched = false
                    endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                }
            }
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let touch = touches.first
        let location = touch?.location(in: self)
        
        if(currentGameState == gameState.inGame)
        {
            if(btnPressed == true)
            {
                let btnSprite = atPoint(location!) as? GameSprite
                
                if(btnSprite?.checkBtnGameSprite() == true && btnSprite?.getGameSpriteNum() == prevNum)
                {
                    currentColorNumber = (btnSprite?.getGameSpriteNum())!
                    
                    moveCount += 1
                    moveLabel.text = "Move : \(moveCount) / \(maxMoveCount)"
                    resetPressTimerFunc(starting: false)
                    
                    updateBtnAndGrid()
                    //do the checking
                    btnPressed = false
                    
                }
            }
        }
        else if(currentGameState == gameState.gameOver)
        {
            if(btnGameOverTouched == true)
            {
                //                print("acc Score : \(accBonusScore)")
                userDefults.set(currentGameLvl, forKey: "gameLvl")
                userDefults.set(accBonusScore, forKey: "accumulatedBonus")
                userDefults.set(secTime, forKey: "elapseTime")
                userDefults.synchronize()
                
//                print("acc Score : \(secTime)")
                
                endGameButton.texture = SKTexture(imageNamed: "btn_continue_01")
                audioPlayer.stop()
                //for future
                if(winGame == true)
                {
                    restartGameLvl()
                }
                else
                {
                    sendHighscore()
                    goToMainMenu()
                }

            }
        }
        else if(currentGameState == gameState.pause)
        {
            tutorialChange()
        }
    }
    
    func updateBtnAndGrid()
    {
        var isFound = false
        
        updateBtnSprite()
        
        repeat
        {
            isFound = false
            progressCount = 0
            for i in 0..<targetRow
            {
                for j in 0..<targetCol
                {
                    if(boolMapOrigin[i][j])
                    {
                        
                        if(i<(targetRow-1))
                        {
                            if(!boolMapOrigin[i+1][j] && mapNumber[i+1][j] == currentColorNumber)
                            {
                                boolMapTemp[i+1][j] = true
                                isFound = true
                            }
                        }
                        
                        if(j<(targetCol - 1))
                        {
                            if(!boolMapOrigin[i][j+1] && mapNumber[i][j+1] == currentColorNumber)
                            {
                                boolMapTemp[i][j+1] = true
                                isFound = true
                            }
                        }
                        
                        if(i != 0)
                        {
                            if(!boolMapOrigin[i-1][j] && mapNumber[i-1][j] == currentColorNumber)
                            {
                                boolMapTemp[i-1][j] = true
                                isFound = true
                            }
                        }
                        if(j != 0)
                        {
                            if(!boolMapOrigin[i][j-1] && mapNumber[i][j-1] == currentColorNumber)
                            {
                                boolMapTemp[i][j-1] = true
                                isFound = true
                            }
                        }
                        
                        boolMapTemp[i][j] = true
                    }
                    
                }
            }
            
            
            for i in 0..<targetRow
            {
                for j in 0..<targetCol
                {
                    boolMapOrigin[i][j] = boolMapTemp[i][j]
                    if(boolMapOrigin[i][j] == true)
                    {
                        mapNumber[i][j] = currentColorNumber
                        progressCount += 1
                    }
                }
            }
            
            
        }while(isFound == true)
        
        var tempCountss = 0
        for x in 0..<mapNumber.count{
            //            var line = ""
            for y in 0..<mapNumber[x].count
            {
                //                line += String(mapNumber[x][y])
                //                line += " "
                
                let temp = mapNumber[x][y]
                mapTemp.remove(at: tempCountss)
                mapTemp.insert(temp, at: tempCountss)
                
                tempCountss += 1
            }
            //            print(line)
        }
        
        let percentage = Int((CGFloat(progressCount) / CGFloat(mapTemp.count)) * 100)
        progressLabel.text = "Progress : \(percentage) / \(100)"
        
        updateMapSprite()
        
        if(progressCount == mapTemp.count && moveCount <= maxMoveCount )
        {
            endGame(gameWin: true)
        }
            
        else if(moveCount == maxMoveCount && progressCount < mapTemp.count)
        {
            endGame(gameWin: false)
        }
        
    }
    
    func updateMapSprite()
    {
        for x in 0..<mapNode.children.count
        {
            if let grid = mapNode.children[x] as? GameSprite
            {
                if(grid.getGameSpriteNum() != mapTemp[x])
                {
                    grid.changeGridNumAndColor(newNumberGrid: mapTemp[x])
                }
            }
        }
    }
    
    func updateBtnSprite()
    {
        var tempCount2 = 1
        
        for child in btnNode.children
        {
            if let spriteNode = child as? GameSprite
            {
                if(tempCount2 == currentColorNumber)
                {
                    spriteNode.changeGameBtnSprite(btnChoosed: true)
                }
                else
                {
                    spriteNode.changeGameBtnSprite(btnChoosed: false)
                }
                tempCount2 += 1
            }
        }
    }
    
    func endGame(gameWin : Bool)
    {
        gameEnd = true
        if(gameWin == true)
        {
            winGame = true
            currentGameLvl += 1
            //            let temp = maxMoveCount - moveCount
            //            secTime += temp
            //            timeLabel.text = timeString(time: TimeInterval(secTime))
        }
        else
        {
            winGame = false
            currentGameLvl = 0
        }
        showGameOver()
    }
    
    func showGameOver()
    {
        pressTimer.invalidate()
        
        currentGameState = gameState.gameOver
        
        gameTimer.invalidate()
        progressPressBar.removeAllActions()
        
        let anim_fromTop : CGFloat = 50
        
        //make the game over ui spawn/shown
        
        let text_top = SKLabelNode(fontNamed: "AllerDisplay")
        text_top.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        text_top.fontSize = 80
        text_top.zPosition = 100
        
        let text_bottom = SKLabelNode(fontNamed: "AllerDisplay")
        text_bottom.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
        text_bottom.fontSize = 80
        text_bottom.zPosition = 100
        
        let workerComputer1 = SKSpriteNode(imageNamed: "nextStage_text")
        workerComputer1.position = CGPoint(x: frame.midX, y: frame.midY + 680 + anim_fromTop)
        workerComputer1.zPosition = 101
        
        endGameButton = SKSpriteNode(imageNamed: "btn_continue_01")
        endGameButton.setScale(1)
        endGameButton.name = "endGameButton"
        endGameButton.zPosition = 101
        
        var shapeSizeX: CGFloat = 1000
        var shapeSizeY: CGFloat = 1000
        
        var shape2SizeX: CGFloat = 1000
        var shape2SizeY: CGFloat = 1000
        
        var shapePositionY: CGFloat = 80
        
        var temp = 0
        if(XpressBtnEnd == false)
        {
            temp = (maxMoveCount - moveCount) * (currentGameLvl + 1)
            
        }
        else
        {
            temp = 0
        }
        
        accBonusScore += temp //accumulatedBonusHere
        let temp2 = temp + secTime
        
        if(winGame == false)
        {
            let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
            temp_msg.text = "you were not quick to adapt\nto change."
            temp_msg.fontSize = 80
            temp_msg.horizontalAlignmentMode = .center
            temp_msg.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
            
            let text_extra = temp_msg.multilined(lineSpacing: CGFloat(10))
            text_extra.position = CGPoint(x: frame.midX, y: frame.midY + 490 + anim_fromTop) // - (text_msg.frame.height/2)
            text_extra.zPosition = 100
            UIGameOverNode.addChild(text_extra)
            
            text_top.text = "Highscore : \(highscore)"
            text_top.position = CGPoint(x: frame.midX, y: frame.midY + 180 + anim_fromTop)
            
            text_bottom.text = "Current : \(accBonusScore + secTime)"
            text_bottom.position = CGPoint(x: frame.midX, y: frame.midY + 80 + anim_fromTop)
            
            workerComputer1.texture = SKTexture(imageNamed: "gameover_text")
            workerComputer1.setScale(1)
            
            let workerComputer = SKSpriteNode(imageNamed: "gameover_icon")
            workerComputer.setScale(1)
            workerComputer.position = CGPoint(x: frame.midX, y: frame.midY - 250 + anim_fromTop)
            workerComputer.zPosition = 101
            UIGameOverNode.addChild(workerComputer)
            
            endGameButton.position = CGPoint(x: frame.midX, y: frame.midY - 670 + anim_fromTop)
            
            shapeSizeX = 1000
            shapeSizeY = 1200
            
            shape2SizeX = 950
            shape2SizeY = 1150
            
            shapePositionY = 80
            
            accBonusScore = 0
            secTime = 0
            run(SKAction.playSoundFileNamed("lose.wav", waitForCompletion: false))
        }
        else
        {
            
            text_top.text = "Bonus : \(temp)"
            text_top.position = CGPoint(x: frame.midX, y: frame.midY + anim_fromTop + 280) //330
            
            text_bottom.text = "Total : \(temp2)"
            text_bottom.position = CGPoint(x: frame.midX, y: frame.midY + anim_fromTop + 180) //130
            
            //            let text_middle = SKLabelNode(fontNamed: "AllerDisplay")
            //            text_middle.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
            //            text_middle.fontSize = 80
            //            text_middle.zPosition = 100
            //            text_middle.text = "Current : \(secTime)"
            //            text_middle.position = CGPoint(x: frame.midX, y: frame.midY + anim_fromTop + 230) //-250
            //            UIGameOverNode.addChild(text_middle)
            
            let choosenMsg = Int(arc4random_uniform(UInt32(msgContinue.count)))
            
            let temp_msg = SKLabelNode(fontNamed: "H.H. Samuel")
            temp_msg.text = "\(msgContinue[choosenMsg])" // 3 - us and in
            temp_msg.fontSize = 80
            temp_msg.horizontalAlignmentMode = .center
            temp_msg.fontColor = UIColor(red: 48/255.0, green: 55/255.0, blue: 131/255.0, alpha: 1.0)
            
            let text_msg = temp_msg.multilined(lineSpacing: CGFloat(7))
            text_msg.position = CGPoint(x: frame.midX, y: frame.midY + anim_fromTop - 60)
            text_msg.zPosition = 100
            UIGameOverNode.addChild(text_msg)
            
            workerComputer1.texture = SKTexture(imageNamed: "nextStage_text")
            workerComputer1.position = CGPoint(x: frame.midX, y: frame.midY + 570 + anim_fromTop)
            workerComputer1.setScale(1)
            
            endGameButton.position = CGPoint(x: frame.midX, y: frame.midY - 580 + anim_fromTop)
            
            shapeSizeX = 1000
            shapeSizeY = 1000
            
            shape2SizeX = 950
            shape2SizeY = 950
            
            shapePositionY = 70
            
            run(SKAction.playSoundFileNamed("win.wav", waitForCompletion: false))
            
        }
        workerComputer1.setScale(1)
        
        UIGameOverNode.addChild(workerComputer1)
        UIGameOverNode.addChild(text_top)
        UIGameOverNode.addChild(text_bottom)
        
        UIGameOverNode.addChild(endGameButton)
        
        let shape = SKShapeNode()
        shape.path = UIBezierPath(roundedRect: CGRect(x: -(shapeSizeX / 2), y: -(shapeSizeY / 2), width: shapeSizeX, height: shapeSizeY), cornerRadius: 30).cgPath
        shape.position = CGPoint(x: frame.midX , y: frame.midY + shapePositionY + anim_fromTop)
        shape.fillColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.strokeColor = UIColor(colorLiteralRed: 211/255, green: 229/255, blue: 245/255, alpha: 1)
        shape.lineWidth = 10
        shape.zPosition = 80
        UIGameOverNode.addChild(shape)
        
        let shape2 = SKShapeNode()
        shape2.path = UIBezierPath(roundedRect: CGRect(x: -(shape2SizeX / 2), y: -(shape2SizeY / 2), width: shape2SizeX , height: shape2SizeY), cornerRadius: 20).cgPath
        shape2.position = CGPoint(x: frame.midX , y: frame.midY + shapePositionY + anim_fromTop)
        shape2.fillColor = UIColor.white
        shape2.strokeColor = UIColor.white
        shape2.lineWidth = 10
        shape2.zPosition = 81
        UIGameOverNode.addChild(shape2)
        
        let moveDown = SKAction.moveBy(x: 0, y: -50, duration: 0.2)
        UIGameOverNode.run(moveDown)
        
        let bgIntro = SKSpriteNode(color: UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50), size: CGSize(width: playAbleWidth, height: self.size.height))
        bgIntro.position = CGPoint(x: frame.midX , y: frame.midY)
        bgIntro.zPosition = 70
        addChild(bgIntro)
    }
    
    
    func goToMainMenu()
    {
        let scene = GameThree(size: self.size) // Change from GameScene -> MainMenuScene class if wan go to Main Menu
        scene.scaleMode = self.scaleMode
        let animation = SKTransition.crossFade(withDuration: 0.5) // Choose any transition you like
        self.view?.presentScene(scene, transition: animation)
    }
    
    func restartGameLvl()
    {
        //code to restart the game
        let scene = GameThree(size: self.size) // Whichever scene you want to restart (and are in)
        scene.scaleMode = self.scaleMode
        let animation = SKTransition.crossFade(withDuration: 0.5) // ...Add transition if you like
        self.view?.presentScene(scene, transition: animation)
    }
    
    func getHighscore() {
        // get highscore from server
//                highscore = "get highscore api"
    }
    
    func sendHighscore() {
        // send highscore to server
//                highscore var api here = highscore
    }
}
