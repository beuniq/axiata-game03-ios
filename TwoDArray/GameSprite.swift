//
//  GameSprite.swift
//  TwoDArray
//
//  Created by Beuniq on 25/07/2017.
//  Copyright © 2017 Beuniq. All rights reserved.
//

import Foundation
import SpriteKit

enum GameSpriteType : String
{
    case grid = "grid",
    gameBtn = "gameBtn",
    gameBtnOverlay = "gameBtnOverlay"
}

class GameSprite : SKSpriteNode
{    
    var gameSpriteRow = 0
    var gameSpriteColumn = 0
    var thisSpriteType : String = ""
    var gameSpriteNum = 0
    
    var shadowFrame = SKSpriteNode(imageNamed: "panel_square")
  
    var textBtn_1 = SKLabelNode(fontNamed: "H.H. Samuel")
    var textBtn_2 = SKLabelNode(fontNamed: "H.H. Samuel")
    
    var btnTextCount = 0
    
    init(spriteRow : Int, spriteColumn : Int, spriteNum : Int, currentSpriteType : GameSpriteType) //, textStringBtn : String, iconBtnTexture : String
    {
        var thisSpriteTexture = SKTexture()
        var thisSpriteSize = thisSpriteTexture.size()
        var tempTextureName = ""
        
        gameSpriteRow = spriteRow
        gameSpriteColumn = spriteColumn
        gameSpriteNum = spriteNum
        thisSpriteType = currentSpriteType.rawValue
        
        if(currentSpriteType == GameSpriteType.grid)
        {
            tempTextureName = "big_square_1"
            thisSpriteSize = CGSize(width: 64, height: 64)
        }
        else if(currentSpriteType == GameSpriteType.gameBtn)
        {
            tempTextureName = "panel_square"
            thisSpriteSize = CGSize(width: 304, height: 304)
        }
        else
        {
            let choosenIconNum = Int(arc4random_uniform(UInt32(GameThree.iconName.count)))
            
            let textChoosen = GameThree.iconName[choosenIconNum]
            let iconChoosen = GameThree.iconSpriteName[choosenIconNum]
            
            tempTextureName = "\(GameThree.iconSpriteName[choosenIconNum])"
            thisSpriteSize = CGSize(width: 304, height: 304)
            
            GameThree.iconName = GameThree.iconName.filter(){$0 != textChoosen}
            GameThree.iconSpriteName = GameThree.iconSpriteName.filter(){$0 != iconChoosen}
            
            let substrings: [String] = textChoosen.components(separatedBy: "\n")
            
            btnTextCount = substrings.count
            
            textBtn_1.text = substrings[0]
            textBtn_1.position = CGPoint(x: 0, y: -95)
            textBtn_1.fontSize = 40
            textBtn_1.zPosition = -1
            
            if(btnTextCount > 1)
            {
                textBtn_2.text = substrings[1]
                textBtn_2.position = CGPoint(x: 0, y: -137)
                textBtn_2.fontSize = 40
                textBtn_2.zPosition = -1
            }
        }
        
        thisSpriteTexture = SKTexture(imageNamed: tempTextureName)
        super.init(texture: thisSpriteTexture, color: .clear , size: thisSpriteSize)
        
        if(currentSpriteType == GameSpriteType.gameBtnOverlay)
        {
            self.addChild(textBtn_1)
            if(btnTextCount > 0)
            {
                self.addChild(textBtn_2)
            }
        }
        if(currentSpriteType == GameSpriteType.grid)
        {
            changeGridNumAndColor(newNumberGrid: self.gameSpriteNum)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getGameSpriteNum() -> Int
    {
        return self.gameSpriteNum
    }
    
    func checkBtnGameSprite() -> Bool
    {
        //if its not button return false
        if(thisSpriteType == GameSpriteType.grid.rawValue)
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func changeGridNumAndColor(newNumberGrid : Int)
    {
        if(thisSpriteType == GameSpriteType.grid.rawValue)
        {
            self.gameSpriteNum = newNumberGrid
            self.color = changeColorSpriteDefault()
            self.colorBlendFactor = 1
        }
    }
    
    func changeGameBtnSprite(btnChoosed : Bool)
    {
        if(btnChoosed == false)
        {
            self.color = UIColor.white
            self.colorBlendFactor = 1
            textBtn_1.fontColor = UIColor.white
            if(btnTextCount > 1)
            {
                textBtn_2.fontColor = UIColor.white
            }
        }
        else
        {
            self.colorBlendFactor = 1
            self.color = changeColorBtnChoosed()
            textBtn_1.fontColor = changeColorBtnChoosed()
            
            if(btnTextCount > 1)
            {
                textBtn_2.fontColor = changeColorBtnChoosed()
            }
        }
    }
    
    
    func changeDefBtn()
    {
        self.color = changeColorSpriteDefault()
        self.colorBlendFactor = 1
        
        shadowFrame.zPosition = -1
        shadowFrame.position = CGPoint(x: 0, y: -10)
        shadowFrame.colorBlendFactor = 1
        shadowFrame.color = UIColor(red: 33/255.0, green: 44/255.0, blue: 91/255.0, alpha: 1.0)
        self.addChild(shadowFrame)
    }
    
    func changeColorSpriteDefault() -> UIColor
    {
        var colorTemp = UIColor()
        if(self.gameSpriteNum == 1)
        {
            //253,73,66
            colorTemp = UIColor(red: 250/255.0, green: 52/255.0, blue: 45/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 2)
        {
            //254,137,71
            colorTemp = UIColor(red: 255/255.0, green: 130/255.0, blue: 60/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 3)
        {
            //93,199,254
            colorTemp = UIColor(red: 52/255.0, green: 152/255.0, blue: 219/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 4)
        {
            //27,215,129
            colorTemp = UIColor(red: 28/255.0, green: 215/255.0, blue: 130/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 5)
        {
            //254,202,61
            colorTemp = UIColor(red: 255/255.0, green: 220/255.0, blue: 62/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 6)
        {
            //254,94,153
            colorTemp = UIColor(red: 255/255.0, green: 104/255.0, blue: 162/255.0, alpha: 1.0)
        }
        
        return colorTemp
    }
    
    func changeColorBtnChoosed() -> UIColor
    {
        var colorTemp = UIColor()
        if(self.gameSpriteNum == 1)
        {
            colorTemp = UIColor(red: 146/255.0, green: 5/255.0, blue: 0/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 2)
        {
            colorTemp = UIColor(red: 157/255.0, green: 56/255.0, blue: 0/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 3)
        {
            colorTemp = UIColor(red: 0/255.0, green: 58/255.0, blue: 97/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 4)
        {
            colorTemp = UIColor(red: 0/255.0, green: 100/255.0, blue: 54/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 5)
        {
            colorTemp = UIColor(red: 137/255.0, green: 91/255.0, blue: 0/255.0, alpha: 1.0)
        }
        else if(self.gameSpriteNum == 6)
        {
            colorTemp = UIColor(red: 161/255.0, green: 0/255.0, blue: 62/255.0, alpha: 1.0)
        }
        
        return colorTemp
    }
    
    
}
