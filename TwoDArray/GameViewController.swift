//
//  GameViewController.swift
//  TwoDArray
//
//  Created by Beuniq on 17/07/2017.
//  Copyright © 2017 Beuniq. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = GameThree(size: CGSize(width: 1536, height: 2048))
        let skViewtest = view as! SKView
        
        skViewtest.showsFPS = false
        skViewtest.showsNodeCount = false
        skViewtest.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill
        skViewtest.presentScene(scene)
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
